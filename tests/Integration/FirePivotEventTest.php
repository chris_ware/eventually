<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration;

use Altek\Eventually\Tests\Database\Factories\ArticleFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\Events\CustomEvent;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Support\Facades\Event;

class FirePivotEventTest extends EventuallyTestCase
{
    /**
     * @test
     */
    public function itDoesNotFireEventsWhenDispatcherIsNotSet(): void
    {
        User::unsetEventDispatcher();

        $user     = UserFactory::new()->create();
        $articles = ArticleFactory::new()->count(2)->create();

        self::assertCount(0, $user->articles()->get());

        self::assertTrue($user->articles()->attach($articles));

        self::assertCount(2, $user->articles()->get());
    }

    /**
     * @test
     */
    public function itPreventsModelsFromBeingAttachedViaCustomEventListener(): void
    {
        $articles = ArticleFactory::new()->count(2)->create();

        $user                           = new class() extends User {
            protected $dispatchesEvents = [
                'attaching' => CustomEvent::class,
            ];
        };

        Event::listen(CustomEvent::class, static function () {
            return false;
        });

        self::assertFalse($user->articles()->attach($articles));
    }
}
